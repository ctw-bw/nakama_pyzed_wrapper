"""
You can put a loop around the retrieve and export_png to export more than one
frame.
"""

import pyzed_wrapper as pw

# Currently assuming a file ./output.svo is present.
# You can modify the filepath in .input_parameters after instance creation
wrapper = pw.Wrapper("svo")
wrapper.open_input_source()
wrapper.retrieve()
wrapper.export_png("./sample/export_sample.png")
wrapper.close_input_source()
