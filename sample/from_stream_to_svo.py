import time
import pyzed_wrapper as pw

wrapper = pw.Wrapper("stream")
wrapper.open_input_source()
wrapper.start_recording()
time.sleep(10)
wrapper.stop_recording()
wrapper.close_input_source()