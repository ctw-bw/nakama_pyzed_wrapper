
import pyzed.sl as sl


class InputParameters(object):
    ID = "id"
    SERIAL = "serial"
    SVO = "svo"
    STREAM = "stream"

    def __init__(self, type="id"):
        self.type = type

        if self.type == self.ID:
            self.id = 0
        elif self.type == self.SERIAL:
            self.serial_number = 0
        elif self.type == self.SVO:
            self.svo_input_filename = "./output.svo"
        elif self.type == self.STREAM:
            self.sender_ip = "10.4.4.30"
            self.port = 30000
        
        return


class ImageRetrievalParameters(object):

    def __init__(self):
        self.view = sl.VIEW.SIDE_BY_SIDE
        self.type = sl.MEM.CPU
        self.resolution = sl.Resolution(0, 0)
        return


    def get(self):
        return {
            "view": self.view,
            "type": self.type,
            "resolution": self.resolution}


class MeasureRetrievalParameters(object):

    def __init__(self):
        self.measure = sl.MEASURE.DEPTH
        self.type = sl.MEM.CPU
        self.resolution = sl.Resolution(0, 0)
        return


    def get(self):
        return {
            "measure": self.measure,
            "type": self.type,
            "resolution": self.resolution}
