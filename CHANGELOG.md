
# release-1.0.2
# release-1.0.1
* Compatible with pyzed API 4.0

# release-0.0.3
* In wrapper_settings, split into image- and measurement retrieval parameters
* Compatible with pyzed API 3.7

# release-0.0.2
* Compatible with pyzed API 3.6

# release-0.0.1
* First release compatible with pyzed API 3.6