# nakama_pyzed wrapper
Python wrapper around the ZED stereo camera python API (pyzed.sl) to collect functionality in a single class. 
The settings that can be modified are part of the Wrapper class (see the __init__ function). Also refer to the stereolabs python documentation for more details.

https://www.stereolabs.com/docs/api/python/

## Dependencies
ZED SDK version 4.0 and its dependencies.
After SDK installation, the Python API should also be installed.

## Installation
Use pip and the pyproject.toml file to install pyzed_wrapper as package.

    $ cd ./src
    $ python3 -m pip install .

## Use
See README.md in src folder, and sample for two minimal examples.

## Docker
To run the container the nvidia-container-toolchain is required. Also see stereolabs documentation. 

Update the information in the (hidden) .env file in ./docker if necessary.
Then use docker and docker compose to build and start a container with everything installed.

    $ cd ./docker
    $ docker compose up 

The container must run with --gpus all and --privileged settings. The compose file should take care of this.

## Docker devcontainer
Use visual studio code to open a devcontainer which mounts the nakama_pyzed_wrapper folder on disk into the Docker container.
* Open the Python folder in visual studio code.
* Press ctrl + shift + P
* Select "Reopen in Container"


### X11 forwarding from container
To provide X11 forwarding on Ubuntu: (only for gl-devel mode)
First run:
xhost +si:localuser:root

Then add the tag:
-e DISPLAY -v /tmp/.X11-unix:/=tmp/.X11-unix
